# EicRootLayoutFiles

Some files for creating EICROOT layouts.
These files need to be put into EICROOT and run via that.
Beampipe used is just an approximation, but the radii of the layers are correct, and that's what mainly matters here. When putting it into Fun4All, everything is correct.

Important to note is that FstGeoParData.cxx has been changed. This lives in /home/eic/eicroot/eic/detectors/maps/ in the Docker container I use. After this has been changed, I do make in /home/eicroot/build/

