#!/bin/bash
#A script to perform the full simulation. simulation, digitization, reconstruction

#Get settings for the simulation
#echo "Enter the PDG code for the particle would you like to simulate (11: electrons, -211: pions, etc)"
#read particle_id

particle_id=211
echo "PID $particle_id" >> "./usedParams.txt"

#echo "Enter the transverse momentum of the tracks in GeV / c (lower bound): "
#read momentum_lower

momentum_lower=0
echo "momentum_lower $momentum_lower" >> "./usedParams.txt"

#echo "Enter the transverse momentum of the tracks in GeV / c (upper bound): "
#read momentum_upper

momentum_upper=5
echo "momentum_upper $momentum_upper" >> "./usedParams.txt"

#echo "Enter the psuedo-rapidity (lower bound): "
#read pr_lower

pr_lower=-0.5
echo "eta_lower $pr_lower" >> "./usedParams.txt"

#echo "Enter the psuedo-rapidity (upper bound): "
#read pr_upper

pr_upper=0.5
echo "eta_upper $pr_upper" >> "./usedParams.txt"

echo "Enter the pixel width in um (assuming square pixels): "
read pix

echo "pix $pix" >> "./usedParams.txt"

#echo "Enter the timing pixel width in um (assuming square pixels): "
#read timingPix
timingPix=20

echo "timingPix $timingPix" >> "./usedParams.txt"

#echo "Enter the number of events you would like to run for (2000 is good): "
#read n_events

n_events=100000
echo "n_events $n_events" >> "./usedParams.txt"

#echo "Would you like to print the results to file (true / false): "
#read to_file

to_file=true

#Run the simulation
root -l "simulation.C($n_events, $momentum_lower, $momentum_upper, $pr_lower, $pr_upper, $particle_id)" #> /dev/null #simulate tracks
root -l "digitization.C($pix, $timingPix)"  > tmp #> /dev/null #digitize hits on detectors (smearing)
root -l reconstruction.C  #> /dev/null #reconstruct tracks from smeared hits
root -l MyAnalysis.C #"build_analysis_tree.C($momentum_lower, $momentum_upper, $pr_lower, $pr_upper, $to_file, $particle_id, $pix)" #compare reconstructions with simulations

#Piping std::out into /dev/null to get rid of it quickly. std::err still shows up.
#Not sure if faster... Maybe it is, maybe not. At least I see all the errors... It's definitely not MUCH faster.
