#include "detector_setup.h"
void reconstruction()
{
  // Load basic libraries;
  gROOT->Macro("$VMCWORKDIR/gconfig/rootlogon.C");  
    
  // Create generic analysis run manager; configure it for track reconstruction;
  EicRunAna *fRun = new EicRunAna();
  fRun->SetInputFile ("simulation.root");
  fRun->AddFriend    ("digitization.root");
  fRun->SetOutputFile("reconstruction.root");

  // Invoke and configure "ideal" PandaRoot tracking code wrapper; 
  EicIdealTrackingCode* idealTracker = new EicIdealTrackingCode();
  #ifdef _SILICON_DISKS_
  idealTracker->AddDetectorGroup("FST");
  idealTracker->AddDetectorGroup("BST");
  #endif
  #ifdef _BARREL_
  idealTracker->AddDetectorGroup("VST");
  #endif
  #ifdef _TIMING_
  idealTracker->AddDetectorGroup("TIMING");
  #endif
  #ifdef _TPC_
  idealTracker->AddDetectorGroup("TPC");
  #endif
  #ifdef _GEM_DISKS_
  idealTracker->AddDetectorGroup("FGT");
  idealTracker->AddDetectorGroup("BGT");
  #endif
  
  //These values do not represent any physical smearing. Instead they simply 
  //make the initial guess for the vertex position and the momentum slightly different
  //from the true values, and then feed that initial guess into the reconstruction
  //code. Usually this initial guess would come from some other algorithm
  //which has identified the track in the first place.s
  idealTracker->SetRelativeMomentumSmearing(0.1);
  idealTracker->SetVertexSmearing(0.01, 0.01, 0.01);
  fRun->AddTask(idealTracker);

  // Invoke and configure PandaRoot Kalman filter code wrapper;
  fRun->AddTask(new EicRecoKalmanTask(idealTracker));

  // This call here just performs track backward propagation to the beam line; 
  fRun->AddTask(new PndPidCorrelator());

  // Assemble event (reconstructed tracks, MC info, etc); (only needed if using a full event, not a single track)
  //fRun->AddTask(new EicEventAssembler());

  // Initialize and run the reconstruction; exit at the end;
  fRun->Run();
} // reconstruction()
