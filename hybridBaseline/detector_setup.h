//Beam pipe

#define _BEAM_PIPE_ //comment out this line for no beam pipe
#define _BEAM_PIPE_THICKNESS_ 0.8 //mm
//#define _BEAM_PIPE_THICKNESS_ 8.0 //mm

//Silicon Barrel

#define _BARREL_ //comment out this line for no silicon barrel

//Timing layer

//#define _TIMING_

//Silicon Disks

#define _SILICON_DISKS_ //comment out this line for no silicon disks

//GEM Disks

//#define _GEM_DISKS_ //comment out this line for no gem disks

//TPC

#define _TPC_ //comment out this line for no tpc

