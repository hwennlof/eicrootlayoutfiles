#!/bin/bash
#A script to construct the detectors and start the event simulator to view the results.

echo "Enter the number corresponding to the barrel configuration you would like to use (See vst.C, or use 2 for the standard BEAST barrel):"
read barrel_id

#Run the simulation
root -l beampipe.C #construct beam pipe
root -l "vst.C($barrel_id)" #construct barrels
mv vst-v01.0-fs.root vstStandard.root
root -l "timing.C" #construct barrels
root -l tpc.C #construct tpc
root -l fbst.C #Construct disks
root -l "simulation.C(1, 10, 120, 11)" #simulate a single track so that the event viewer can be opened
root -l eventDisplay.C #Open event display
