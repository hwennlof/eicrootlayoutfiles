#include "detector_setup.h"

#define _PI_ 3.14159265358979

//Here we want momentum, not transverse momentum
void simulation(int nEvents = 2000, Double_t momentum_lower = 5.0, Double_t momentum_upper = 5.0, Double_t pr_lower = -0.5, Double_t pr_upper = -0.5, int PDG = 11)
{
  // Load basic libraries;
  gROOT->Macro("$VMCWORKDIR/gconfig/rootlogon.C");

  // Create simulation run manager; use GEANT3 for tracking excercises here;
  EicRunSim *fRun = new EicRunSim("TGeant3");
  fRun->SetOutputFile("simulation.root");

  // Suppress secondary particle production (work with primary electrons); 
  fRun->SuppressSecondaries();

  // Beam pipe; 
  #ifdef _BEAM_PIPE_
  cout << "beam pipe" << endl;
  fRun->AddModule(new EicDummyDetector("BEAMPIPE", "./beampipe.root"));
  #endif

  // Silicon detectors;
  #ifdef _BARREL_
  cout << "barrel" << endl;
  fRun->AddModule(new EicMaps("VST", "./vstStandard.root", qVST));
  #endif
  
  #ifdef _TIMING_
  cout << "timingLayer" << endl;
  //last argument is detector type, so let's keep that the same.
  //Apparently doesn't matter...
  //
  fRun->AddModule(new EicMaps("TIMING", "./timing-v01.0-fs.root", qVST));
  #endif
  
  #ifdef _SILICON_DISKS_
  cout << "si disks" << endl;
  fRun->AddModule(new EicMaps("FST", "./fst-v01.0-ss.root", qFST));
  fRun->AddModule(new EicMaps("BST", "./bst-v01.0-ss.root", qBST));
  #endif
  
  // GEM disks;
  #ifdef _GEM_DISKS_
  cout << "gem" << endl;
  fRun->AddModule(new EicGem ("FGT", "GEM/fgt-v01.0.root",  qFGT));
  fRun->AddModule(new EicGem ("BGT", "GEM/bgt-v01.0.root",  qBGT));
  #endif
  
  // TPC;
  #ifdef _TPC_
  cout << "tpc" << endl;
  fRun->AddModule(new EicTpc (       "./tpc-test.root"));
  #endif
  
  // Create and set up physics Event Generator;
  {
	
	
	Double_t theta_lower = 2.0*atan(exp(-1.0*pr_lower))*180.0/_PI_;
	Double_t theta_upper = 2.0*atan(exp(-1.0*pr_upper))*180.0/_PI_;
	
	//Double_t theta_lower_rads = 2.0*atan(exp(-1.0*pr_lower)); //In radians now
	//Double_t theta_upper_rads = 2.0*atan(exp(-1.0*pr_upper));
	
	//momentum_lower = transv_momentum_lower/(sin(theta_lower_rads));
	//momentum_upper = transv_momentum_upper/(sin(theta_upper_rads));
	std::cout << "Theta_upper: " << theta_upper << " Upper: " << momentum_upper << " sin of it: " << sin(theta_upper) << std::endl;
	//Good! Now this works, for inputting p_T instead of p!
	
    //EicBoxGeneratorHW *boxGen = new EicBoxGeneratorHW(PDG); 
    //boxGen->SetTransvMomentumRange(transv_momentum_lower, transv_momentum_upper);
    //boxGen->SetThetaRange(theta_lower, theta_upper); //Wants it in degrees
	
	//Want to use a uniform distribution in momentum, rather than transverse momentum.
    EicBoxGeneratorHWUniformEta *boxGen = new EicBoxGeneratorHWUniformEta(PDG); 
    boxGen->SetMomentumRange(momentum_lower, momentum_upper);
    //boxGen->SetThetaRange(theta_lower, theta_upper); //Wants it in degrees
    boxGen->SetEtaRange(pr_lower, pr_upper);
    
    
    //boxGen->SetEtaRange(pr_lower, pr_upper); //Could use this directly, if it had inherited from FairBoxGenerator rather than FairGenerator...
	//FairBoxGenerator even exists here, under /home/eic/eicroot/generators/FairBoxGenerator.h
	
    fRun->AddGenerator(boxGen);
  }

  // Create and set up the Apr'2016 solenoid field;
  //fRun->AddField(new PndSolenoidMap("SolenoidMap1", "R")); //3 T uniform
  fRun->AddField(new PndSolenoidMap("SolenoidMap2", "R")); //1.5 T uniform

  // Initialize and run the simulation; exit at the end;
  fRun->Run(nEvents);
} // simulation()
