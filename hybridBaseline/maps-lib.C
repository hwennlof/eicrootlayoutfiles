
//
//  Crappy, but perhaps the easiest;
//

void DefinitionParser(void *ptr)
{
  MapsGeoParData *maps = (MapsGeoParData*)ptr;

  // Simpliest case wins;
#ifdef _BEAM_LINE_UNIFORM_GEOMETRY_
  maps->SetGeometryType(EicGeoParData::SimpleStructure);
#endif
#ifdef _NO_STRUCTURE_GEOMETRY_
  maps->SetGeometryType(EicGeoParData::NoStructure);
#endif

#ifdef _USE_TRIANGULAR_ASSEMBLIES_
  maps->UseTriangularAssemblies(true);
#endif

#ifdef _TEST_VERSION_
  maps->SetTestGeometryFlag();
#endif

  // Let MapsGeoParData base class instance know, which elements are wanted;
  // this set of #define and respective call looks strange, but visually it 
  // is more convenient to check #define statements at the beginning of the 
  // script rather than to check several lines with functional calls in the text;
#ifdef _WITH_MOUNTING_RINGS_
  maps->WithMountingRings(true);
#endif
#ifdef _WITH_ENFORCEMENT_BRACKETS_
  maps->WithEnforcementBrackets(true);
#endif
#ifdef _WITH_EXTERNAL_PIPES_
  maps->WithExternalPipes(true);
#endif
} // DefinitionParser()


void SetMapsColors(void *ptr)
{
  EicNamePatternHub<Color_t> *ctable = ((EicGeoParData*)ptr)->GetColorTable();

  // Specify color preferences; NB: order of calls matters!;
  ctable->AddPatternMatch("WaterPipe",      kYellow);
  ctable->AddPatternMatch("Water",          kBlue);
  ctable->AddPatternMatch("StaveBracket",   kOrange);
  ctable->AddPatternMatch("Beam",           kBlack);
  ctable->AddPatternMatch("ColdPlate",      kYellow);
  ctable->AddPatternMatch("MimosaCore",     kYellow);
  ctable->AddPatternMatch("CellFlexLayer",  kGreen+2);
  ctable->AddPatternMatch("AluStrips",      kGray);
  ctable->AddPatternMatch("MountingRing",   kMagenta+1);
} // SetMapsColors()


void *ConfigureAliceCell()
{
  // First want to cook the basic building block - Mimosa chip assembly; the
  // rest is composed out of these blocks in a LEGO fashion;
  MapsMimosaAssembly *ibcell = new MapsMimosaAssembly();
  
  //  !!!!!!
  //
  // NB: these parameters were carefully tuned to match ALICE ITS upgrade design;
  //     do NOT touch unless you understand what you are doing;
  //
  //  !!!!!!

  // Air container volume parameters sufficient to pack all the stuff;
  ibcell->mAssemblyBaseWidth           =   17.500;
  ibcell->mAssemblySideSlope           =   30.000;
  ibcell->mChipToChipGap               =    0.100;

  // Space structure;
  ibcell->mApexEnforcementBeamDiameter =    0.400;
  ibcell->mEnforcementStripWidth       =    0.500;
  ibcell->mEnforcementStripThickness   =    0.200;
  ibcell->mBaseEnforcementBeamWidth    =    1.200;
  ibcell->mSideWallThickness           =    0.050;

  // Basic Mimosa 34 chip parameters; pixel size does not matter here (will play 
  // a role during digitization only);
  ibcell->mChipLength                  =   30.000;
  ibcell->mChipWidth                   =   15.000;
  // Well, ALICE rad.length scan plot indicates, that it is equal to the chip width;
  // NB: do NOT try to set this value to be larger than mChipWidth (unless modify
  // assembly and stove width calculation):
  ibcell->mAssemblyDeadMaterialWidth   =   15.000;
  ibcell->mChipThickness               =    0.050;
  ibcell->mChipActiveZoneThickness     =    0.018;
  ibcell->mChipDeadAreaWidth           =    2.000;

  // Layers at the base of the assembly; kapton and effective alu thinckness;
  ibcell->mFlexCableKaptonThickness    =    0.100;
  ibcell->mFlexCableAluThickness       =    0.050;
  ibcell->mColdPlateThickness          =    0.265;
  //These values taken from Table 4.1 in the ALICE ITS TDR (page 55).
  //Gives close to 0.3% (0.28%). We'll use this for the inner ones.

  // Water pipes; assume 2 parallel pipes; 25um thick walls;
  ibcell->mWaterPipeInnerDiameter      =    1.024;
  ibcell->mWaterPipeWallThickness      =    0.025;

  return ibcell;
} // ConfigureAliceCell()



void *ConfigureAliceCellOuter()
{
  // First want to cook the basic building block - Mimosa chip assembly; the
  // rest is composed out of these blocks in a LEGO fashion;
  MapsMimosaAssembly *obcell = new MapsMimosaAssembly();
  
  //  !!!!!!
  //
  // NB: these parameters were carefully tuned to match ALICE ITS upgrade design;
  //     do NOT touch unless you understand what you are doing;
  //
  //  !!!!!!

  // Air container volume parameters sufficient to pack all the stuff;
  obcell->mAssemblyBaseWidth           =   70.000;
  obcell->mAssemblySideSlope           =   30.000;
  obcell->mChipToChipGap               =    0.100;

  // Space structure;
  obcell->mApexEnforcementBeamDiameter =    0.400;
  obcell->mEnforcementStripWidth       =    0.500;
  obcell->mEnforcementStripThickness   =    0.200;
  obcell->mBaseEnforcementBeamWidth    =    1.200;
  obcell->mSideWallThickness           =    0.050;

  // Basic Mimosa 34 chip parameters; pixel size does not matter here (will play 
  // a role during digitization only);
  obcell->mChipLength                  =   30.000;
  obcell->mChipWidth                   =   60.000;
  // Well, ALICE rad.length scan plot indicates, that it is equal to the chip width;
  // NB: do NOT try to set this value to be larger than mChipWidth (unless modify
  // assembly and stove width calculation):
  obcell->mAssemblyDeadMaterialWidth   =   60.000;
  obcell->mChipThickness               =    0.050;
  obcell->mChipActiveZoneThickness     =    0.018;
  obcell->mChipDeadAreaWidth           =    2.000;

  // Layers at the base of the assembly; kapton and effective alu thinckness;
  obcell->mFlexCableKaptonThickness    =    0.600;
  obcell->mFlexCableAluThickness       =    0.300;
  obcell->mColdPlateThickness          =    0.440;
  //Loosely based on Table 4.2 of the ALICE ITS TDR. Trimmed to be 0.8% of X0

  // Water pipes; assume 2 parallel pipes; 25um thick walls;
  obcell->mWaterPipeInnerDiameter      =    1.024;
  obcell->mWaterPipeWallThickness      =    0.025;

  return obcell;
} // ConfigureAliceCell()



void *ConfigureTimingCell()
{
  // First want to cook the basic building block - Mimosa chip assembly; the
  // rest is composed out of these blocks in a LEGO fashion;
  MapsMimosaAssembly * timingCell = new MapsMimosaAssembly();
  
  //  !!!!!!
  //
  // NB: these parameters were carefully tuned to match ALICE ITS upgrade design;
  //     do NOT touch unless you understand what you are doing;
  //
  //  !!!!!!

  // Air container volume parameters sufficient to pack all the stuff;
  timingCell->mAssemblyBaseWidth           =   70.000;
  timingCell->mAssemblySideSlope           =   30.000;
  timingCell->mChipToChipGap               =    0.100;

  // Space structure;
  timingCell->mApexEnforcementBeamDiameter =    0.400;
  timingCell->mEnforcementStripWidth       =    0.500;
  timingCell->mEnforcementStripThickness   =    0.200;
  timingCell->mBaseEnforcementBeamWidth    =    1.200;
  timingCell->mSideWallThickness           =    0.050;

  // Basic Mimosa 34 chip parameters; pixel size does not matter here (will play 
  // a role during digitization only);
  timingCell->mChipLength                  =   30.000;
  timingCell->mChipWidth                   =   60.000;
  // Well, ALICE rad.length scan plot indicates, that it is equal to the chip width;
  // NB: do NOT try to set this value to be larger than mChipWidth (unless modify
  // assembly and stove width calculation):
  timingCell->mAssemblyDeadMaterialWidth   =   60.000;
  timingCell->mChipThickness               =    0.050;
  timingCell->mChipActiveZoneThickness     =    0.018;
  timingCell->mChipDeadAreaWidth           =    2.000;

  // Layers at the base of the assembly; kapton and effective alu thinckness;
  timingCell->mFlexCableKaptonThickness    =    1.200;
  timingCell->mFlexCableAluThickness       =    0.660;
  timingCell->mColdPlateThickness          =    0.880;
  //Trimmed to be twice that of obcell. So 1.6% of X0

  // Water pipes; assume 2 parallel pipes; 25um thick walls;
  timingCell->mWaterPipeInnerDiameter      =    1.024;
  timingCell->mWaterPipeWallThickness      =    0.025;

  return timingCell;
}



void *ConfigureFatTimingCell()
{
  // First want to cook the basic building block - Mimosa chip assembly; the
  // rest is composed out of these blocks in a LEGO fashion;
  MapsMimosaAssembly * timingCell = new MapsMimosaAssembly();
  
  //  !!!!!!
  //
  // NB: these parameters were carefully tuned to match ALICE ITS upgrade design;
  //     do NOT touch unless you understand what you are doing;
  //
  //  !!!!!!

  // Air container volume parameters sufficient to pack all the stuff;
  timingCell->mAssemblyBaseWidth           =   62.500;
  timingCell->mAssemblySideSlope           =   30.000;
  timingCell->mChipToChipGap               =    0.100;

  // Space structure;
  timingCell->mApexEnforcementBeamDiameter =    0.400;
  timingCell->mEnforcementStripWidth       =    0.500;
  timingCell->mEnforcementStripThickness   =    0.200;
  timingCell->mBaseEnforcementBeamWidth    =    1.200;
  timingCell->mSideWallThickness           =    0.050;

  // Basic Mimosa 34 chip parameters; pixel size does not matter here (will play 
  // a role during digitization only);
  timingCell->mChipLength                  =   50.000;
  timingCell->mChipWidth                   =   60.000;
  // Well, ALICE rad.length scan plot indicates, that it is equal to the chip width;
  // NB: do NOT try to set this value to be larger than mChipWidth (unless modify
  // assembly and stove width calculation):
  timingCell->mAssemblyDeadMaterialWidth   =   60.000;
  timingCell->mChipThickness               =    0.050;
  timingCell->mChipActiveZoneThickness     =    0.018;
  timingCell->mChipDeadAreaWidth           =    2.000;

  // Layers at the base of the assembly; kapton and effective alu thinckness;
  timingCell->mFlexCableKaptonThickness    =    2.500;
  timingCell->mFlexCableAluThickness       =    1.345;
  timingCell->mColdPlateThickness          =    1.700;
  //Trimmed to be twice that of obcell. So 3.2% of X0

  // Water pipes; assume 2 parallel pipes; 25um thick walls;
  timingCell->mWaterPipeInnerDiameter      =    1.024;
  timingCell->mWaterPipeWallThickness      =    0.025;

  return timingCell;
}


void *ConfigureITS3Cell()
{
  // First want to cook the basic building block - Mimosa chip assembly; the
  // rest is composed out of these blocks in a LEGO fashion;
  MapsMimosaAssembly * its3cell = new MapsMimosaAssembly();

  // Air container volume parameters sufficient to pack all the stuff;
  its3cell->mAssemblyBaseWidth           =   17.500;
  its3cell->mAssemblySideSlope           =   30.000;
  its3cell->mChipToChipGap               =    0.100;

  // Space structure;
  //Don't want any here
  its3cell->mApexEnforcementBeamDiameter =    0.00001;
  its3cell->mEnforcementStripWidth       =    0.00001;
  its3cell->mEnforcementStripThickness   =    0.00001;
  its3cell->mBaseEnforcementBeamWidth    =    0.00001;
  its3cell->mSideWallThickness           =    0.00001;

  // Basic Mimosa 34 chip parameters; pixel size does not matter here (will play 
  // a role during digitization only);
  its3cell->mChipLength                  =   30.000;
  its3cell->mChipWidth                   =   15.000;
  // Well, ALICE rad.length scan plot indicates, that it is equal to the chip width;
  // NB: do NOT try to set this value to be larger than mChipWidth (unless modify
  // assembly and stove width calculation):
  its3cell->mAssemblyDeadMaterialWidth   =   15.000;
  its3cell->mChipThickness               =    0.022; //Still 50 um of silicon.
  its3cell->mChipActiveZoneThickness     =    0.018;
  its3cell->mChipDeadAreaWidth           =    2.000;

  // Layers at the base of the assembly; kapton and effective alu thinckness;
  //Don't want any, here, basically. Guess we'll need cable though
  its3cell->mFlexCableKaptonThickness    =    0.00001;
  its3cell->mFlexCableAluThickness       =    0.00001;
  its3cell->mColdPlateThickness          =    0.00001;

  // Water pipes. Don't want any.
  its3cell->mWaterPipeInnerDiameter      =    0.00001;
  its3cell->mWaterPipeWallThickness      =    0.00001;

  return its3cell;
} // ConfigureITS3Cell()

//For ITS3-like staves
void *ConfigureITS3OuterCell()
{
  // First want to cook the basic building block - Mimosa chip assembly; the
  // rest is composed out of these blocks in a LEGO fashion;
  MapsMimosaAssembly * its3outercell = new MapsMimosaAssembly();

  // Air container volume parameters sufficient to pack all the stuff;
  its3outercell->mAssemblyBaseWidth           =   70.000;
  its3outercell->mAssemblySideSlope           =   30.000;
  its3outercell->mChipToChipGap               =    0.100;

  // Space structure;
  its3outercell->mApexEnforcementBeamDiameter =    0.400;
  its3outercell->mEnforcementStripWidth       =    0.500;
  its3outercell->mEnforcementStripThickness   =    0.200;
  its3outercell->mBaseEnforcementBeamWidth    =    1.200;
  its3outercell->mSideWallThickness           =    0.050;

  // Basic Mimosa 34 chip parameters; pixel size does not matter here (will play 
  // a role during digitization only);
  its3outercell->mChipLength                  =   30.000;
  its3outercell->mChipWidth                   =   60.000;
  // Well, ALICE rad.length scan plot indicates, that it is equal to the chip width;
  // NB: do NOT try to set this value to be larger than mChipWidth (unless modify
  // assembly and stove width calculation):
  its3outercell->mAssemblyDeadMaterialWidth   =   60.000;
  its3outercell->mChipThickness               =    0.050;
  its3outercell->mChipActiveZoneThickness     =    0.018;
  its3outercell->mChipDeadAreaWidth           =    2.000;

  // Layers at the base of the assembly; kapton and effective alu thinckness;
  its3outercell->mFlexCableKaptonThickness    =    0.400;
  its3outercell->mFlexCableAluThickness       =    0.220;
  its3outercell->mColdPlateThickness          =    0.220;
  //Loosely based on Table 4.2 of the ALICE ITS TDR. Trimmed to be 0.8% of X0

  // Water pipes; assume 2 parallel pipes; 25um thick walls;
  its3outercell->mWaterPipeInnerDiameter      =    1.024;
  its3outercell->mWaterPipeWallThickness      =    0.025;


  return its3outercell;
} // ConfigureITS3OuterCell()



void * ConfigureAliceDiskCell()
{
  // First want to cook the basic building block - Mimosa chip assembly; the
  // rest is composed out of these blocks in a LEGO fashion;
  MapsMimosaAssembly *ibcell = new MapsMimosaAssembly();
  
  //  !!!!!!
  //
  // NB: these parameters were carefully tuned to match ALICE ITS upgrade design;
  //     do NOT touch unless you understand what you are doing;
  //
  //  !!!!!!

  // Air container volume parameters sufficient to pack all the stuff;
  ibcell->mAssemblyBaseWidth           =   17.500;
  ibcell->mAssemblySideSlope           =   30.000;
  ibcell->mChipToChipGap               =    0.100;

  // Space structure;
  ibcell->mApexEnforcementBeamDiameter =    0.400;
  ibcell->mEnforcementStripWidth       =    0.500;
  ibcell->mEnforcementStripThickness   =    0.200;
  ibcell->mBaseEnforcementBeamWidth    =    1.200;
  ibcell->mSideWallThickness           =    0.050;

  // Basic Mimosa 34 chip parameters; pixel size does not matter here (will play 
  // a role during digitization only);
  ibcell->mChipLength                  =   10.000;
  ibcell->mChipWidth                   =   15.000;
  // Well, ALICE rad.length scan plot indicates, that it is equal to the chip width;
  // NB: do NOT try to set this value to be larger than mChipWidth (unless modify
  // assembly and stove width calculation):
  ibcell->mAssemblyDeadMaterialWidth   =   15.000;
  ibcell->mChipThickness               =    0.050;
  ibcell->mChipActiveZoneThickness     =    0.018;
  ibcell->mChipDeadAreaWidth           =    2.000;

  // Layers at the base of the assembly; kapton and effective alu thinckness;
  ibcell->mFlexCableKaptonThickness    =    0.100;
  ibcell->mFlexCableAluThickness       =    0.050;
  ibcell->mColdPlateThickness          =    0.265;
  //These values taken from Table 4.1 in the ALICE ITS TDR (page 55).
  //Gives close to 0.3% (0.28%). We'll use this for the inner ones.

  // Water pipes; assume 2 parallel pipes; 25um thick walls;
  ibcell->mWaterPipeInnerDiameter      =    1.024;
  ibcell->mWaterPipeWallThickness      =    0.025;

  return ibcell;
} // ConfigureAliceCell()

//For ITS3-like disks, 0.24% X/X0
void * ConfigureAliceITS3DiskCell()
{
  // First want to cook the basic building block - Mimosa chip assembly; the
  // rest is composed out of these blocks in a LEGO fashion;
  MapsMimosaAssembly *ibcell = new MapsMimosaAssembly();
  
  //  !!!!!!
  //
  // NB: these parameters were carefully tuned to match ALICE ITS upgrade design;
  //     do NOT touch unless you understand what you are doing;
  //
  //  !!!!!!

  // Air container volume parameters sufficient to pack all the stuff;
  ibcell->mAssemblyBaseWidth           =   17.500;
  ibcell->mAssemblySideSlope           =   30.000;
  ibcell->mChipToChipGap               =    0.100;

  // Space structure;
  ibcell->mApexEnforcementBeamDiameter =    0.400;
  ibcell->mEnforcementStripWidth       =    0.500;
  ibcell->mEnforcementStripThickness   =    0.200;
  ibcell->mBaseEnforcementBeamWidth    =    1.200;
  ibcell->mSideWallThickness           =    0.050;

  // Basic Mimosa 34 chip parameters; pixel size does not matter here (will play 
  // a role during digitization only);
  ibcell->mChipLength                  =   10.000;
  ibcell->mChipWidth                   =   15.000;
  // Well, ALICE rad.length scan plot indicates, that it is equal to the chip width;
  // NB: do NOT try to set this value to be larger than mChipWidth (unless modify
  // assembly and stove width calculation):
  ibcell->mAssemblyDeadMaterialWidth   =   15.000;
  ibcell->mChipThickness               =    0.050;
  ibcell->mChipActiveZoneThickness     =    0.018;
  ibcell->mChipDeadAreaWidth           =    2.000;

  // Layers at the base of the assembly; kapton and effective alu thinckness;
  ibcell->mFlexCableKaptonThickness    =    0.100;
  ibcell->mFlexCableAluThickness       =    0.050;
  ibcell->mColdPlateThickness          =    0.190;
  

  // Water pipes; assume 2 parallel pipes; 25um thick walls;
  ibcell->mWaterPipeInnerDiameter      =    1.024;
  ibcell->mWaterPipeWallThickness      =    0.025;

  return ibcell;
} // ConfigureAliceITS3DiskCell()
